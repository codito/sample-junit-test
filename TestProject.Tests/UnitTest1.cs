using System.IO;

namespace TestProject.Tests;

[TestClass]
public class UnitTest1
{
    public TestContext TestContext { get; set; }

    [TestMethod]
    public void TestMethod1()
    {
        Console.WriteLine("{2010CAE3-7BC0-4841-A5A3-7D5F947BB9FB}");
        Console.WriteLine("{998AC9EC-7429-42CD-AD55-72037E7AF3D8}");

        var file = Path.Combine(Path.GetTempPath(), "y.txt");
        if (!File.Exists(file))
        {
            File.Create(file);
        }
        TestContext.AddResultFile(file);
    }

    [TestMethod]
    [TestProperty("Category", "Fail")]
    public void TestMethod2()
    {
        Console.WriteLine("{Fail1}");

        var file = Path.Combine(Path.GetTempPath(), "fail.txt");
        if (!File.Exists(file))
        {
            File.Create(file);
        }
        TestContext.AddResultFile(file);

        Assert.AreEqual(2, 1);
    }
}