dotnet build
dotnet tool install dotnet-coverage
dotnet tool run dotnet-coverage collect -f cobertura -o TestProject.Tests/TestResults/report.cobertura.xml "dotnet test --no-build --logger:junit"
